package dataAccessObject;

import model.Car;
import controller.calculateIpva;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class CarDaoImplTest implements calculateIpva {

    private Car car;
    private CarDao carDao;
    private List<Car> dataCar;

    public CarDaoImplTest() {
        carDao = new CarDaoImpl();
    }

//    @Test
    public void testSave() throws Exception {
        System.out.println("Salvar:");
        car = new Car(null, "5", "Uno", "Fiat", "943267837689", "MXT-5246", 15000);
        calculateIpva();
        carDao.save(car);
    }

//    @Test
    public void testChange() throws Exception {
        System.out.println("Alterar:");
        int id = 22;
        car = new Car(id, "3", "Opala", "Chevrolet", "9689", "OPA-0100", 100);
        calculateIpva();
        carDao.change(car);
    }

//    @Test
    public void testDelete() throws Exception {
        System.out.println("Deletar:");
        int id = 4;
        carDao.delete(id);
    }

//    @Test
    public void testListAll() throws Exception {
        System.out.println("Listar Todos:");
        dataCar = carDao.listAll();
        for (Car car1 : dataCar) {
            System.out.println("ID: " + car1.getId());
            System.out.println("Numero de Portas: " + car1.getDoorNumber());
            System.out.println("Tipo: " + car1.getType());
            System.out.println("Modelo: " + car1.getModel());
            System.out.println("Fabricante: " + car1.getBrand());
            System.out.println("Placa: " + car1.getPlate());
            System.out.println("Valor: " + car1.getValue());
            System.out.println("IPVA: " + car1.getIpva());
            System.out.println("Renavam: " + car1.getRenavam());
            System.out.println();
        }
    }

//    @Test
    public void testSearchById() throws Exception {
        System.out.println("Pesquisar por ID:");
        int id = 5;
        car = (Car) carDao.searchById(id);
        System.out.println("ID: " + car.getId());
        System.out.println("Numero de Portas: " + car.getDoorNumber());
        System.out.println("Tipo: " + car.getType());
        System.out.println("Modelo: " + car.getModel());
        System.out.println("Fabricante: " + car.getBrand());
        System.out.println("Placa: " + car.getPlate());
        System.out.println("Valor: " + car.getValue());
        System.out.println("IPVA: " + car.getIpva());
        System.out.println("Renavam: " + car.getRenavam());
    }

//    @Test
    public void testSearchByModel() throws Exception {
        System.out.println("Pesquisar por Modelo:");
        String model = "a";
        dataCar = carDao.searchByModel(model);
        for (Car car1 : dataCar) {
            System.out.println("ID: " + car1.getId());
            System.out.println("Numero de Portas: " + car1.getDoorNumber());
            System.out.println("Tipo: " + car1.getType());
            System.out.println("Modelo: " + car1.getModel());
            System.out.println("Fabricante: " + car1.getBrand());
            System.out.println("Placa: " + car1.getPlate());
            System.out.println("Valor: " + car1.getValue());
            System.out.println("IPVA: " + car1.getIpva());
            System.out.println("Renavam: " + car1.getRenavam());
            System.out.println();
        }
    }

//    @Test
    public void testSearchByBrand() throws Exception {
        System.out.println("Pesquisar por Marca:");
        String brand = "fi";
        dataCar = carDao.searchByBrand(brand);
        for (Car car1 : dataCar) {
            System.out.println("ID: " + car1.getId());
            System.out.println("Numero de Portas: " + car1.getDoorNumber());
            System.out.println("Tipo: " + car1.getType());
            System.out.println("Modelo: " + car1.getModel());
            System.out.println("Fabricante: " + car1.getBrand());
            System.out.println("Placa: " + car1.getPlate());
            System.out.println("Valor: " + car1.getValue());
            System.out.println("IPVA: " + car1.getIpva());
            System.out.println("Renavam: " + car1.getRenavam());
            System.out.println();
        }
    }
    
//    @Test
    public void testSearchByIpva() throws Exception {
        System.out.println("Pesquisar por IPVA:");
        double ipva = 200.0;
        dataCar = carDao.searchByIpva(ipva);
        for (Car car1 : dataCar) {
            System.out.println("ID: " + car1.getId());
            System.out.println("Numero de Portas: " + car1.getDoorNumber());
            System.out.println("Tipo: " + car1.getType());
            System.out.println("Modelo: " + car1.getModel());
            System.out.println("Fabricante: " + car1.getBrand());
            System.out.println("Placa: " + car1.getPlate());
            System.out.println("Valor: " + car1.getValue());
            System.out.println("IPVA: " + car1.getIpva());
            System.out.println("Renavam: " + car1.getRenavam());
            System.out.println();
        }
    }
    
//    @Test
    public void testSearchByBrandAndModel() throws Exception {
        System.out.println("Pesquisar por Marca e Modelo:");
        String brand = "fiat";
        String model = "uno";
        dataCar = carDao.searchByBrandAndModel(brand, model);
        for (Car car1 : dataCar) {
            System.out.println("ID: " + car1.getId());
            System.out.println("Numero de Portas: " + car1.getDoorNumber());
            System.out.println("Tipo: " + car1.getType());
            System.out.println("Modelo: " + car1.getModel());
            System.out.println("Fabricante: " + car1.getBrand());
            System.out.println("Placa: " + car1.getPlate());
            System.out.println("Valor: " + car1.getValue());
            System.out.println("IPVA: " + car1.getIpva());
            System.out.println("Renavam: " + car1.getRenavam());
            System.out.println();
        }
    }
    
    @Override
    public void calculateIpva() {
        car.setIpva(car.getValue() * 0.03);
    }

}
