package dataAccessObject;

import model.Motorcycle;
import controller.calculateIpva;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class MotorcycleDaoImplTest implements calculateIpva {

    private Motorcycle motorcycle;
    private MotorcycleDao motorDao;
    private List<Motorcycle> dataMotor;

    public MotorcycleDaoImplTest() {
        motorDao = new MotorcycleDaoImpl();
    }

//    @Test
    public void testSave() throws Exception {
        System.out.println("Salvar");
        motorcycle = new Motorcycle(null, "125", "Bis", "Honda", "423489", "CAS-2424", 5000);
        calculateIpva();
        motorDao.save(motorcycle);
    }

//    @Test
    public void testChange() throws Exception {
        System.out.println("Alterar:");
        int id = 7;
        motorcycle = new Motorcycle(id, "250", "CB", "Honda", "41431454", "GAA-1838", 10000);
        calculateIpva();
        motorDao.change(motorcycle);

    }

//    @Test
    public void testDelete() throws Exception {
        System.out.println("Deletar");
        int id = 6;
        motorDao.delete(id);

    }

//   @Test
    public void testListAll() throws Exception {
        System.out.println("Listar Todos:");
        dataMotor = motorDao.listAll();
        for (Motorcycle motorcycle1 : dataMotor) {
            System.out.println("ID: " + motorcycle1.getId());
            System.out.println("Potencia: " + motorcycle1.getPower());
            System.out.println("Tipo: " + motorcycle1.getType());
            System.out.println("Modelo: " + motorcycle1.getModel());
            System.out.println("Fabricante: " + motorcycle1.getBrand());
            System.out.println("Placa: " + motorcycle1.getPlate());
            System.out.println("Valor: " + motorcycle1.getValue());
            System.out.println("IPVA: " + motorcycle1.getIpva());
            System.out.println("Renavam: " + motorcycle1.getRenavam());
            System.out.println();
        }
    }

//   @Test
    public void testSearchById() throws Exception {
        System.out.println("Pesquisar por ID:");
        int id = 12;
        motorcycle = (Motorcycle) motorDao.searchById(id);
        System.out.println("ID: " + motorcycle.getId());
        System.out.println("Potencia: " + motorcycle.getPower());
        System.out.println("Tipo: " + motorcycle.getType());
        System.out.println("Modelo: " + motorcycle.getModel());
        System.out.println("Fabricante: " + motorcycle.getBrand());
        System.out.println("Placa: " + motorcycle.getPlate());
        System.out.println("Valor: " + motorcycle.getValue());
        System.out.println("IPVA: " + motorcycle.getIpva());
        System.out.println("Renavam: " + motorcycle.getRenavam());

    }

//   @Test
    public void testSearchByModel() throws Exception {
        System.out.println("Pesquisar por Modelo:");
        String model = "b";
        dataMotor = motorDao.searchByModel(model);
        for (Motorcycle motorcycle1 : dataMotor) {
            System.out.println("ID: " + motorcycle1.getId());
            System.out.println("Potencia: " + motorcycle1.getPower());
            System.out.println("Tipo: " + motorcycle1.getType());
            System.out.println("Modelo: " + motorcycle1.getModel());
            System.out.println("Fabricante: " + motorcycle1.getBrand());
            System.out.println("Placa: " + motorcycle1.getPlate());
            System.out.println("Valor: " + motorcycle1.getValue());
            System.out.println("IPVA: " + motorcycle1.getIpva());
            System.out.println("Renavam: " + motorcycle1.getRenavam());
            System.out.println();
        }
    }
    
//    @Test
    public void testSearchByBrand() throws Exception {
        System.out.println("Pesquisar por Marca:");
        String brand = "h";
        dataMotor = motorDao.searchByBrand(brand);
        for (Motorcycle motorcycle1 : dataMotor) {
            System.out.println("ID: " + motorcycle1.getId());
            System.out.println("Potencia: " + motorcycle1.getPower());
            System.out.println("Tipo: " + motorcycle1.getType());
            System.out.println("Modelo: " + motorcycle1.getModel());
            System.out.println("Fabricante: " + motorcycle1.getBrand());
            System.out.println("Placa: " + motorcycle1.getPlate());
            System.out.println("Valor: " + motorcycle1.getValue());
            System.out.println("IPVA: " + motorcycle1.getIpva());
            System.out.println("Renavam: " + motorcycle1.getRenavam());
            System.out.println();
        }
    }

//    @Test
    public void testSearchByIpva() throws Exception {
        System.out.println("Pesquisar por IPVA:");
        double ipva = 50.0;
        dataMotor = motorDao.searchByIpva(ipva);
        for (Motorcycle motorcycle1 : dataMotor) {
            System.out.println("ID: " + motorcycle1.getId());
            System.out.println("Potencia: " + motorcycle1.getPower());
            System.out.println("Tipo: " + motorcycle1.getType());
            System.out.println("Modelo: " + motorcycle1.getModel());
            System.out.println("Fabricante: " + motorcycle1.getBrand());
            System.out.println("Placa: " + motorcycle1.getPlate());
            System.out.println("Valor: " + motorcycle1.getValue());
            System.out.println("IPVA: " + motorcycle1.getIpva());
            System.out.println("Renavam: " + motorcycle1.getRenavam());
            System.out.println();
        }
    }
    
    @Test
    public void testSearchByBrandAndModel() throws Exception {
        System.out.println("Pesquisar por Marca e Modelo:");
        String brand = "Honda";
        String model = "Bis";
        dataMotor = motorDao.searchByBrandAndModel(brand, model);
        for (Motorcycle motorcycle1 : dataMotor) {
            System.out.println("ID: " + motorcycle1.getId());
            System.out.println("Potencia: " + motorcycle1.getPower());
            System.out.println("Tipo: " + motorcycle1.getType());
            System.out.println("Modelo: " + motorcycle1.getModel());
            System.out.println("Fabricante: " + motorcycle1.getBrand());
            System.out.println("Placa: " + motorcycle1.getPlate());
            System.out.println("Valor: " + motorcycle1.getValue());
            System.out.println("IPVA: " + motorcycle1.getIpva());
            System.out.println("Renavam: " + motorcycle1.getRenavam());
            System.out.println();
        }
    }

    
    @Override
    public void calculateIpva() {
        motorcycle.setIpva(motorcycle.getValue() * 0.02);
    }

}
