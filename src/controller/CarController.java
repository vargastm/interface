package controller;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import dataAccessObject.CarDao;
import dataAccessObject.CarDaoImpl;
import java.io.Serializable;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Car;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class CarController implements calculateIpva, Serializable {

    private CarDao carDao;
    private Car car;

    public void saveCar(Car car) {
        this.car = car;
        carDao = new CarDaoImpl();
        try {
            calculateIpva();
            carDao.save(car);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(car.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(car.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    public void changeCar(Car car) {
        this.car = car;
        carDao = new CarDaoImpl();
        try {
            calculateIpva();
            carDao.change(car);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(car.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(car.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    @Override
    public void calculateIpva() {
        car.setIpva(car.getValue() * 0.03);
    }
}
