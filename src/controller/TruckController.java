package controller;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import dataAccessObject.TruckDao;
import dataAccessObject.TruckDaoImpl;
import java.io.Serializable;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Truck;

/**
 *
 * @author Cassiano
 */
public class TruckController implements calculateIpva, Serializable {

    private TruckDao truckDao;
    private Truck truck;

    public void saveTruck(Truck truck) {
        this.truck = truck;
        truckDao = new TruckDaoImpl();
        try {
            calculateIpva();
            truckDao.save(truck);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(truck.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(truck.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    public void changeTruck(Truck truck) {
        this.truck = truck;
        truckDao = new TruckDaoImpl();
        try {
            calculateIpva();
            truckDao.change(truck);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(truck.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().toString().contains(truck.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    @Override
    public void calculateIpva() {
        truck.setIpva(truck.getValue() * 0.04);
    }

}
