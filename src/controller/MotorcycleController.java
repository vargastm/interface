package controller;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import dataAccessObject.MotorcycleDao;
import dataAccessObject.MotorcycleDaoImpl;
import java.io.Serializable;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Motorcycle;

/**
 *
 * @author Cassiano
 */
public class MotorcycleController implements calculateIpva, Serializable {

    private MotorcycleDao motorDao;
    private Motorcycle motorcycle;

    public void saveMotorcycle(Motorcycle motorcycle) {
        this.motorcycle = motorcycle;
        motorDao = new MotorcycleDaoImpl();
        try {
            calculateIpva();
            motorDao.save(motorcycle);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(motorcycle.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(motorcycle.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    public void changeMotorcycle(Motorcycle motorcycle) {
        this.motorcycle = motorcycle;
        motorDao = new MotorcycleDaoImpl();
        try {
            calculateIpva();
            motorDao.change(motorcycle);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(motorcycle.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(motorcycle.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    @Override
    public void calculateIpva() {
        motorcycle.setIpva(motorcycle.getValue() * 0.02);
    }

}
