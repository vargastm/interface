package controller;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import dataAccessObject.BusDao;
import dataAccessObject.BusDaoImpl;
import java.io.Serializable;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Bus;

/**
 *
 * @author Cassiano
 */
public class BusController implements calculateIpva, Serializable {

    private BusDao busDao;
    private Bus bus;

    public void saveBus(Bus bus) {
        this.bus = bus;
        busDao = new BusDaoImpl();
        try {
            calculateIpva();
            busDao.save(bus);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(bus.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(bus.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    public void changeBus(Bus bus) {
        this.bus = bus;
        busDao = new BusDaoImpl();
        try {
            calculateIpva();
            busDao.change(bus);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            if (ex.getMessage().contains(bus.getPlate())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com essa placa");
            }
            if (ex.getMessage().contains(bus.getRenavam())) {
                JOptionPane.showMessageDialog(null, "Já existe atomóvel com esse renavam");
            }
        } catch (SQLException e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        } catch (Exception e) {
            System.out.println("erro ao salvar o automovel" + e.getMessage());
            e.getStackTrace();
        }
    }

    @Override
    public void calculateIpva() {
        bus.setIpva(bus.getValue() * 0.05);
    }
}
