package dataAccessObject;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public interface VehicleDao extends BaseDao {

    public abstract List searchByModel(String model) throws SQLException;
    public abstract List searchByBrand(String brand) throws SQLException;
    public abstract List searchByIpva(Double ipva) throws SQLException;
    public abstract List searchByBrandAndModel(String brand, String model) throws SQLException;
}
