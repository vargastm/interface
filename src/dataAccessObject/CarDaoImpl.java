package dataAccessObject;

import model.Car;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class CarDaoImpl extends VehicleDaoImpl implements CarDao, Serializable {

    private List<Car> dataCar;

    @Override
    public void save(Object object) throws SQLException, Exception{
        Car car = (Car) object;
        super.save(car);
        String query = "INSERT INTO Car(doorNumber, idVehicle) VALUES (?,?)";
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, car.getDoorNumber());
            prepared.setInt(2, car.getId());
            prepared.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println("Erro ao salvar automovel " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }

    }

    @Override
    public void change(Object object) throws SQLException {
        Car car = (Car) object;
        super.change(car);
        String query = "UPDATE car SET doorNumber = ? WHERE idVehicle = ?";
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, car.getDoorNumber());
            prepared.setInt(2, car.getId());
            prepared.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao alterar automovel " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        super.delete(id);
    }

    @Override
    public List listAll() throws SQLException {
        dataCar = new ArrayList<>();
        Car car;
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle ";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            result = prepared.executeQuery();
            while (result.next()) {
                car = new Car();
                car.setId(result.getInt("car.idVehicle"));
                car.setDoorNumber(result.getString("doorNumber"));
                car.setType(result.getString("type"));
                car.setModel(result.getString("model"));
                car.setBrand(result.getString("brand"));
                car.setPlate(result.getString("plate"));
                car.setValue(result.getDouble("value"));
                car.setIpva(result.getDouble("ipva"));
                car.setRenavam(result.getString("renavam"));
                dataCar.add(car);
            }
        } catch (Exception e) {
            System.out.println("Erro ao listar todos os automoveis " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataCar;
    }

    @Override
    public Object searchById(int id) throws SQLException {
        Car car = new Car();
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle WHERE vehicle.id = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, id);
            result = prepared.executeQuery();
            result.next();
            car.setId(result.getInt("car.idVehicle"));
            car.setDoorNumber(result.getString("doorNumber"));
            car.setType(result.getString("type"));
            car.setModel(result.getString("model"));
            car.setBrand(result.getString("brand"));
            car.setPlate(result.getString("plate"));
            car.setValue(result.getDouble("value"));
            car.setIpva(result.getDouble("ipva"));
            car.setRenavam(result.getString("renavam"));
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar automovel por ID " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return car;
    }

    @Override
    public List searchByModel(String model) throws SQLException {
        dataCar = new ArrayList<>();
        Car car;
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle WHERE model LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + model + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                car = new Car();
                car.setId(result.getInt("car.idVehicle"));
                car.setDoorNumber(result.getString("doorNumber"));
                car.setType(result.getString("type"));
                car.setModel(result.getString("model"));
                car.setBrand(result.getString("brand"));
                car.setPlate(result.getString("plate"));
                car.setValue(result.getDouble("value"));
                car.setIpva(result.getDouble("ipva"));
                car.setRenavam(result.getString("renavam"));
                dataCar.add(car);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar automovel por modelo " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataCar;
    }

    @Override
    public List searchByBrand(String brand) throws SQLException {
        dataCar = new ArrayList<>();
        Car car;
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle WHERE brand LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + brand + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                car = new Car();
                car.setId(result.getInt("car.idVehicle"));
                car.setDoorNumber(result.getString("doorNumber"));
                car.setType(result.getString("type"));
                car.setModel(result.getString("model"));
                car.setBrand(result.getString("brand"));
                car.setPlate(result.getString("plate"));
                car.setValue(result.getDouble("value"));
                car.setIpva(result.getDouble("ipva"));
                car.setRenavam(result.getString("renavam"));
                dataCar.add(car);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar automovel por marca " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataCar;
    }

    @Override
    public List searchByIpva(Double ipva) throws SQLException {
        dataCar = new ArrayList<>();
        Car car;
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle WHERE ipva > ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setDouble(1, ipva);
            result = prepared.executeQuery();
            while (result.next()) {
                car = new Car();
                car.setId(result.getInt("car.idVehicle"));
                car.setDoorNumber(result.getString("doorNumber"));
                car.setType(result.getString("type"));
                car.setModel(result.getString("model"));
                car.setBrand(result.getString("brand"));
                car.setPlate(result.getString("plate"));
                car.setValue(result.getDouble("value"));
                car.setIpva(result.getDouble("ipva"));
                car.setRenavam(result.getString("renavam"));
                dataCar.add(car);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar automovel por marca " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataCar;
    }

    @Override
    public List searchByBrandAndModel(String brand, String model) throws SQLException {
        dataCar = new ArrayList<>();
        Car car;
        String query = "SELECT * FROM vehicle INNER JOIN car ON vehicle.id = car.idVehicle WHERE brand LIKE ? AND model LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + brand + "%");
            prepared.setString(2, "%" + model + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                car = new Car();
                car.setId(result.getInt("car.idVehicle"));
                car.setDoorNumber(result.getString("doorNumber"));
                car.setType(result.getString("type"));
                car.setModel(result.getString("model"));
                car.setBrand(result.getString("brand"));
                car.setPlate(result.getString("plate"));
                car.setValue(result.getDouble("value"));
                car.setIpva(result.getDouble("ipva"));
                car.setRenavam(result.getString("renavam"));
                dataCar.add(car);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar automovel por marca " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataCar;
    }

}
