package dataAccessObject;

import model.Bus;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class BusDaoImpl extends VehicleDaoImpl implements BusDao, Serializable {

    private Bus bus;
    private List<Bus> dataBus;

    @Override
    public void save(Object object) throws SQLException, Exception {
        bus = (Bus) object;
        super.save(bus);
        String query = "INSERT INTO bus (seat, idVehicle) VALUES (?,?)";
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, bus.getSeat());
            prepared.setInt(2, bus.getId());
            prepared.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao salvar ônibus " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
    }

    @Override
    public void change(Object object) throws SQLException {
        bus = (Bus) object;
        super.change(bus);
        String query = "UPDATE bus SET seat = ? WHERE idVehicle = ?";
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, bus.getSeat());
            prepared.setInt(2, bus.getId());
            prepared.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao alterar ônibus " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        super.delete(id);
    }

    @Override
    public List listAll() throws SQLException {
        dataBus = new ArrayList<>();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            result = prepared.executeQuery();
            while (result.next()) {
                bus = new Bus();
                bus.setId(result.getInt("bus.idVehicle"));
                bus.setSeat(result.getString("seat"));
                bus.setType(result.getString("type"));
                bus.setModel(result.getString("model"));
                bus.setBrand(result.getString("brand"));
                bus.setPlate(result.getString("plate"));
                bus.setValue(result.getDouble("value"));
                bus.setIpva(result.getDouble("ipva"));
                bus.setRenavam(result.getString("renavam"));
                dataBus.add(bus);
            }
        } catch (Exception e) {
            System.out.println("Erro ao listar ônibus " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataBus;
    }

    @Override
    public Object searchById(int id) throws SQLException {
        bus = new Bus();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle WHERE vehicle.id = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, id);
            result = prepared.executeQuery();
            result.next();
            bus.setId(result.getInt("bus.idVehicle"));
            bus.setSeat(result.getString("seat"));
            bus.setType(result.getString("type"));
            bus.setModel(result.getString("model"));
            bus.setBrand(result.getString("model"));
            bus.setPlate(result.getString("plate"));
            bus.setValue(result.getDouble("value"));
            bus.setIpva(result.getDouble("ipva"));
            bus.setRenavam(result.getString("renavam"));
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar ônibus por ID " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return bus;
    }

    @Override
    public List searchByModel(String model) throws SQLException {
        dataBus = new ArrayList<>();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle WHERE model LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + model + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                bus = new Bus();
                bus.setId(result.getInt("bus.idVehicle"));
                bus.setSeat(result.getString("seat"));
                bus.setType(result.getString("type"));
                bus.setModel(result.getString("model"));
                bus.setBrand(result.getString("brand"));
                bus.setPlate(result.getString("plate"));
                bus.setValue(result.getDouble("value"));
                bus.setIpva(result.getDouble("ipva"));
                bus.setRenavam(result.getString("renavam"));
                dataBus.add(bus);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar ônibus por ID modelo " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataBus;
    }

    @Override
    public List searchByBrand(String brand) throws SQLException {
        dataBus = new ArrayList<>();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle WHERE brand LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + brand + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                bus = new Bus();
                bus.setId(result.getInt("bus.idVehicle"));
                bus.setSeat(result.getString("seat"));
                bus.setType(result.getString("type"));
                bus.setModel(result.getString("model"));
                bus.setBrand(result.getString("brand"));
                bus.setPlate(result.getString("plate"));
                bus.setValue(result.getDouble("value"));
                bus.setIpva(result.getDouble("ipva"));
                bus.setRenavam(result.getString("renavam"));
                dataBus.add(bus);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar ônibus por ID marca " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataBus;
    }

    @Override
    public List searchByIpva(Double ipva) throws SQLException {
        dataBus = new ArrayList<>();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle WHERE ipva > ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setDouble(1, ipva);
            result = prepared.executeQuery();
            while (result.next()) {
                bus = new Bus();
                bus.setId(result.getInt("bus.idVehicle"));
                bus.setSeat(result.getString("seat"));
                bus.setType(result.getString("type"));
                bus.setModel(result.getString("model"));
                bus.setBrand(result.getString("brand"));
                bus.setPlate(result.getString("plate"));
                bus.setValue(result.getDouble("value"));
                bus.setIpva(result.getDouble("ipva"));
                bus.setRenavam(result.getString("renavam"));
                dataBus.add(bus);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar ônibus por ID ipva " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataBus;
    }

    @Override
    public List searchByBrandAndModel(String brand, String model) throws SQLException {
        dataBus = new ArrayList<>();
        String query = "SELECT * FROM vehicle INNER JOIN bus ON vehicle.id = bus.idVehicle WHERE brand LIKE ? AND model LIKE ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + brand + "%");
            prepared.setString(2, "%" + model + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                bus = new Bus();
                bus.setId(result.getInt("bus.idVehicle"));
                bus.setSeat(result.getString("seat"));
                bus.setType(result.getString("type"));
                bus.setModel(result.getString("model"));
                bus.setBrand(result.getString("brand"));
                bus.setPlate(result.getString("plate"));
                bus.setValue(result.getDouble("value"));
                bus.setIpva(result.getDouble("ipva"));
                bus.setRenavam(result.getString("renavam"));
                dataBus.add(bus);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar ônibus por ID marca e modelo " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared, result);
        }
        return dataBus;
    }
}
